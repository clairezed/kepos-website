---
layout: page
title-first: La SCIC
title-last: Kèpos
permalink: "/kepos.html"
category: kepos
order: 1
short_description: Kèpos est une aventure coopérative et d’intérêt collectif, thématisée
  sur la transition écologique.
in_homepage: true

---
Kèpos est une Société Coopérative d’Intérêt Collectif (SCIC) réunissant des acteurs économiques du territoire nancéien engagés dans la transition. A travers ce statut, Kèpos fait le choix de la coopération en vue de contribuer à un objectif d’intérêt général, la mise en oeuvre de la transition écologique.