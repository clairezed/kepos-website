---
layout: page
title-first: Nous
title-last: contacter
permalink: "/contact.html"
category: contact
short_description: ''
in_homepage: false
order: 

---
Kèpos est une SCIC dont le siège est à Vandoeuvre-lès-Nancy, dans la métropole de Nancy.

{% include contact-block.html %}

### Blog

Kèpos anime un blog autour des questions de transition écologique, disponible à l’adresse suivante : [**transition-ecologique.org**](https://transition-ecologique.org/)

<iframe class="mj-w-res-iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://app.mailjet.com/widget/iframe/5qIv/ygq" width="100%"></iframe>