---
title-first: Nos
title-last: membres
order: 2
short_description: Notre jardin d’entreprises regroupe une vingtaine d’acteurs économiques
  de secteurs, métiers et marchés différents.
download:
  link: "/jardin-annuaire-kepos-v22032022.pdf"
  button_title: Téléchargez l'annuaire des membres

---
Notre collectif est formé de jeunes entreprises ou associations, la plupart créées depuis moins de cinq ans, qui sont toutes engagées dans un processus de transition, à un titre ou à un autre : stratégie marketing, process utilisés, nature des produits ou des services vendus… Nos activités sont tout à fait hétérogènes : c’est ce qui nous permet de construire des synergies. Nous sommes majoritairement implantés autour de Nancy.

Retrouvez ci-dessous qui nous sommes et qui nous représente au sein de Kèpos :

<div class="columns is-multiline">
{% for item in site.data.membres %}
  <div class="column is-half">
  {% include member.html member=item %}
  </div>
{% endfor %}
</div>