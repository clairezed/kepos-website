---
title-first: Nous
title-last: rejoindre
order: 3
short_description: Si vous êtes d'ores et déjà actif dans la transition, devenez membre
  de notre réseau coopératif et engagé pour vous développer au sein d'un groupe complémentaire
  et ambitieux.
download:
  button_title: Téléchargez le détails de nos services aux membres
  link: "/jardin-offre-pour-les-membres-v221025.pdf"

---
## Vous êtes en mouvement, nous aussi !

Nous sommes en constante recherche de complémentarité, de synergie et d'inspiration. En tant qu'entrepreneur et acteur de la transition écologique dans la coopérative Kèpos, vous pourrez vous inscrire dans un collectif dynamique, structurant et en cohérence avec vos valeurs, dans les paroles et dans les actes.

Nous souhaitons créer un groupe cohérent et à taille humaine. Les membres de Kèpos se connaissent, se conseillent et s'entraident. Leurs offres commerciales se complètent et s'articulent pour toucher les cibles les plus diverses et pour porter le plus loin possible les actions, les compétences et les prestations répondant aux enjeux climatiques.

#### La vie active au jardin

Les membres du « Jardin d’entreprises » s’impliquent et prennent des responsabilités dans la vie du jardin d’entreprises. Ils sont associés de la SCIC dans un collège qui leur est propre, celui des bénéficiaires. Celui-ci possède 46 % des droits de vote en assemblée générale, et 4 membres sur 9 dans le Conseil coopératif de la SCIC.

Ils valorisent leur appartenance au Jardin d’entreprises dans leur communication et leur relation avec ses parties prenantes. Ils sont les ambassadeurs de tous les autres membres. Ils agissent dans le Jardin dans un esprit d’entraide.

#### Vous souhaitez nous rejoindre ?

1. [**Contactez-nous**](https://www.kepos.fr/contact.html "Contact") pour nous dire qui vous êtes, ce qui vous anime et ce qui vous amène jusqu'à nous.
2. Après accord des membres du jardin, venez à la rencontre du collectif et bénéficiez gratuitement de trois mois de découverte de la coopérative en participant aux réunions collectives mensuelles.
3. Passés ces trois mois, si vous confirmez votre candidature, le collectif devra valider votre entrée dans le jardin. Cette validation se fait par consentement (personne ne s’oppose).
4. Signez la convention d'appartenance au Jardin. La souscription d’au moins une part sociale au capital de la SCIC est obligatoire, son montant est de 100 €. Vous contribuerez ensuite trimestriellement au fonctionnement du Jardin en fonction de votre activité et de votre marge. La contribution minimale est fixée à 20 euros HT par mois et son plafond est de 100 euros HT mensuel.
5. Grandissez avec Kèpos, partagez avec les membres, œuvrez à la transition écologique avec nous !