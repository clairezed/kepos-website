---
title-first: Vous
title-last: sensibiliser
order: 1
short_description: Nos ateliers de découverte sont un premier éclairage sur les questions
  de transition et forment une porte ouverte vers le changement.
download:
  button_title: Téléchargez notre offre de sensibilisation
  link: "/sensibilisation-catalogue-v200701.pdf"

---
## Notre offre de sensibilisation

**_Sensibiliser_** _: Rendre sensible, réceptif, attentif à quelque chose._

Les enjeux climatiques et les perturbations qu'ils induisent dans nos sociétés ne sont pas chose aisée à appréhender. Notre offre partagée d'ateliers ludiques, légers, variés, permettront donc à tous les publics de s'ouvrir aux thématiques de leurs choix et d'aller à la découverte des compétences et de la passion des acteurs du territoire qui incarnent le changement dans les domaines suivants :

* Découvrir son environnement
* Jardiner
* Se nourrir
* Fabriquer
* S'émerveiller

Téléchargez notre catalogue ci-dessous et [**sollicitez-nous**](https://www.kepos.fr/contact.html "Contact") pour en savoir plus.