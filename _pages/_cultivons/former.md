---
title-first: Vous
title-last: former
order: 2
short_description: Nous intervenons auprès de vous et de vos équipes pour développer
  et consolider votre démarche de transition et les compétences nécessaires associées.
download:
  button_title: Téléchargez notre offre de formation
  link: "/scic-kepos-catalogue-formation-v221213.pdf"

---
## Notre offre de Formation

**_Former_** _: Développer des connaissances, compétences, habitudes, manières, qualités._

Opérer un changement nécessite le développement puis la maîtrise de nouvelles aptitudes et la mise en oeuvre de nouvelles organisations. Construire le changement en se formant, c'est donc poser les fondations solides pour que votre structure ou vos initiatives traversent le temps et les crises qui seront de plus en plus fréquentes.

En tant qu'Organisme de Formation, nous vous proposons de vous accompagner dans votre progression vers la transition écologique.  Nous abordons aujourd'hui les thèmes suivants :

* le développement territorial ou économique
* La vie de l'entreprise
* L'alimentation
* La biodiversité
* Les sols

**Certifiés Datadock depuis juin 2020 et Qualiopi depuis décembre 2020**, nous avons à cœur de vous offrir des formations riches et actives avec des formateurs membres de Kèpos en constante recherche de nouveauté. **Nos clients évaluent nos sessions de formation à 8,6/10**. **Le taux de satisfaction de nos stagiaires est de 94%** sur 434 avis exprimés _(note supérieure ou égale à 7/10, dernière mise à jour : janvier 2023)._

## Un engagement réciproque

Nous soutenons la sobriété. Il ne s'agit pas seulement de Développement Durable et encore moins de "Greenwashing" : il faut penser refonte de l'écosystème, innovations sociales et sociétales et dynamique à long terme. Œuvrer à sa transition écologique, c'est s'engager pour l'avenir.

En tant qu'organisation, votre impact écologique est plus important que celui d'un seul individu. Les leviers d'actions dans vos mains sont plus puissants qu'imaginé et il est temps de **faire la différence en explorant les pistes de transformation en profondeur** : sans s'effrayer, sans se perdre, avec confiance et volonté.

## Notre catalogue

Téléchargez notre offre de formation ci-dessous ! Dans ce catalogue, vous trouverez l'ensemble des informations dont vous aurez besoin, [**sollicitez-nous**](https://www.kepos.fr/contact.html "Contact") pour en savoir plus et pour élaborer le parcours de formation qui s'adaptera le mieux à vos besoins. Vous pouvez consulter nos Conditions Générales de Ventes [ici](https://drive.kepos.fr/index.php/s/X6TLe3fC6kdkt7x "CGV Kèpos").

Si un aménagement spécifique de la formation est nécessaire (cas de personnes en situation de handicap, illettrisme, difficultés autres, …), n'hésitez pas à nous contacter un mois avant la date souhaitée de début de formation. Localement, nous travaillons étroitement avec l’association Handi Interim, favorisant l’employabilité et l’insertion professionnelle des personnes en situation handicap : [https://handi-interim.org/](https://handi-interim.org/ "https://handi-interim.org/")