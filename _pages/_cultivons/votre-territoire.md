---
title-first: Votre
title-last: territoire
order: 2
short_description: La transition écologique est un projet global et fédérateur à l’échelle
  d’un territoire. Nous vous donnons les clés pour son élaboration et sa mise en oeuvre.
published: false

---
Nous accompagnons les territoires et leurs acteurs dans leur propres transition écologique. En ce sens, nous sommes susceptibles d’intervenir auprès de collectivités et de leurs groupements pour les aider à définir une trajectoire de transition écologique et citoyenne globale.

## La transition écologique comme projet de territoire

Notre approche consiste à ne pas présenter la transition écologique comme devant donner lieu à des ajustements des paramètres des politiques publiques, mais comme une redéfinition systémique de ces mêmes politiques. Les questions dont nous sommes les relais peuvent être les suivantes :

* Quelle politique de mobilité doit être mise en place à l’heure de la descente énergétique ?
* Quelle politique d’aménagement sont souhaitables dans un contexte d’effondrement de la biodiversité ?
* Quelle politique foncière définir alors que les sols sont en train de s’épuiser à vitesse accélérée ?
* Quelle politique d’habitat promouvoir alors que les scenarii de transition énergétique impliquent une diminution de 50% de la demande d’énergie ?
* Quel développement économique mettre en œuvre, alors que la limitation du réchauffement climatique vient radicalement interroger les logiques de développement qui ont prévalu ces 200 dernières années ?
* Comment refaire de la politique ensemble à l’échelle d’un territoire alors que les choix qui doivent être faits sont les plus engageants que l’humanité ait jamais été mise en demeure de faire ?

On le voit, les enjeux sont majeurs et les risques de perte de cohésion sociale ou économique sont très importants pour un territoire. Notre propos est dès lors de vous assister pour sortir de la gestion “as usual” et enrichir le questionnement politique.

### Notre offre de formation

Nous sommes susceptibles de former élus et techniciens d’une collectivité, et parties prenantes d’un territoire, autour de deux thématiques :

* Crise systémique et transition écologique : de quoi parle-t-on ?
* Initier la transition écologique d’un territoire.

La première formation propose, sur une journée, un éclairage de premier niveau sur l’anthropocène et ses différentes dimensions (réchauffement climatique, 6ème extinction des espèces, épuisement des ressources…) en les mettant en relation avec le cadre politique, économique et social dans lequel évoluent nos sociétés contemporaines. Elle donne les concepts et outils intellectuels de base pour comprendre les crises en cours et resituer l’urgence de la transition écologique. Elle définit cette dernière et dessine les perspectives de sa mise en œuvre.

La deuxième formation, toujours sur une journée, travaille sur l’appropriation par les acteurs du territoire des impératifs de la transition écologique. Elle transfère les principes d’action indispensables à un projet de transition, axé sur la sobriété, la limitation des impacts et la résilience. Elle fournit des outils de mobilisation collective à mettre en place à l’échelle d’un territoire. Elle rappelle les principes essentiels liés à la transition dans chaque politique sectorielle.

### Notre offre de conseil

La transition écologique concerne l’ensemble des politiques publiques mises en œuvre sur un territoire. En ce sens, elle ne relève pas d’un enfermement sectoriel, mais est éminemment transversale. Parmi les thématiques sur lesquels nous sommes susceptibles d’intervenir, citons :

* **La mise en perspective des tendances à l’œuvre, internes ou externes, sur le territoire** : ce travail prospectif va au-delà du simple diagnostic de territoire. Il en modifie les enjeux en resituant la trajectoire territoriale dans un contexte systémique bien plus vaste (énergétique, écologique, économique, social, géopolitique…).
* **La définition du projet de territoire** : la transition écologique est d’abord un projet de territoire, qui nécessite un partage des enjeux systémiques qui se posent à toute communauté humaine, une mise en récit d’un futur désirable dans un contexte en plein bouleversement, et la définition d’objectifs partagés à la fois très ambitieux, réalistes et rassembleurs. C’est là où s’incarne la volonté politique qui impulse une démarche de transition.
* **La mise en route démocratique de la transition écologique.** Celle-ci implique, pour un territoire et ses acteurs, de savoir refaire société pour décider ensemble d’une transformation complète et globale. En ce sens, la simple consultation des citoyens est insuffisante : il importe de mettre en œuvre une véritable démocratie de construction. Il s’agit certes là d’un processus au long cours, mais indispensable pour que la transition soit véritablement inclusive.
* **La contribution à l’élaboration des politiques sectorielles** (urbanisme, développement économique, habitat, mobilité, cohésion sociale…). Nous souhaitons pouvoir faire irriguer les principes de la transition dans l’ensemble de ces décisions. Notre ambition est que ces politiques puissent faire système et gagnent en cohérence, et qu’elles relèvent d’une véritable ambition transformatrice, et pas seulement de simple choix gestionnaires.

Sur tous ces aspects, nous vous accompagnons, en faisant dialoguer vos spécificités territoriales avec les grands enjeux systémiques. Notre souhait est qu’à terme, votre territoire puisse s’adapter à des conditions de vie en changement rapide et proposer à ses habitants et forces vives un cadre humain et soutenable.