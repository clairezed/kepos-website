---
layout: page
title-first: "Inscription newsletter"
title-last: 'réussie'
permalink: "/inscription-newsletter-reussie.html"
in_homepage: false
order: 
short_description: ''

---
Félicitations, votre inscription à la newsletter de Kèpos est maintenant effective !

[Revenir à l'accueil]({{ "/" | relative_url }})