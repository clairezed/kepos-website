---
title-first: Nos
title-last: positions
order: 4
short_description: Nous construisons notre vision ensemble, à l'écoute des autres
  et de l'actualité. Retrouvez nos positions, nos avis, nos humeurs dans notre blog.
Document téléchargeable:
  Texte bouton: ''
  Document: ''

---
Nos idées et nos mouvements sont à l'intersection de trois valeurs :

* **Coopération** : Aide, entente, méthode d'action harmonisée entre les membres d'un groupe qui s'associent en vue d'un but commun, la transition écologique.
* **Sobriété** : Tempérance, modération et mesure dans nos manières de vivre et de gérer nos entreprises pour permettre un avenir soutenable sur notre planète.
* **Espérance** : Disposition de l'âme qui porte l'humanité à considérer et désirer un changement important qu'il croit pouvoir se réaliser dans l'avenir. Agissons et croyons en la portée de nos actes.

Et nous prenons position. En tant qu'acteur local du changement, nous vous proposons de retrouver dans notre blog [**Transition-ecologique.org**](https://transition-ecologique.org/ "Transition écologique") l'ensemble des réflexions, des rencontres, des échanges qui font de Kèpos un organisme vivant qui évolue avec le temps, l'actualité et la température.