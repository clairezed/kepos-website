---
title-first: Notre
title-last: équipe
order: 3
short_description: Notre équipe réunit des professionnels engagés et conscients des
  enjeux environnementaux en cours.

---
Kèpos est porté par une équipe engagée et ambitieuse, profondément marquée par les enjeux écologiques, qui cherche à proposer des chemins de transformation à son territoire et ses acteurs.

## Emmanuel Paul

Initiateur et coordonnateur de Kèpos

_"Professionnel confirmé du développement territorial et de l’accompagnement de projets entrepreneuriaux, je porte aujourd’hui la dynamique Kèpos, autour de la transition écologique des entreprises et des territoires. En tant que père de famille, professionnel engagé et citoyen attentif, je suis interpellé par la puissance des enjeux écologiques auxquels l’humanité se trouve confrontée. Ceux-ci, par leur aspect systémique, dessinent un horizon qui doit radicalement questionner nos projets et nos actions. C’est la position que je  défends personnellement et professionnellement en posant la question de leur propre transition écologique à toute une série d'acteurs (porteurs de projets, entreprises, territoires ou institutions), afin de convertir hommes et organisations à la sobriété et à la responsabilité."_

## Laure Hammerer

Consultante en transition écologique

_"C'est avec conviction et enthousiasme que je m'engage aujourd'hui dans l'accompagnement à la transition écologique. Les défis environnementaux auxquels nous faisons face doivent être adressés par des personnes lucides, engagées et dynamiques. J'ai à coeur de convaincre l'ensemble des acteurs de notre société qu'il est nécessaire dès maintenant de faire grandir leurs organisations vers une dynamique plus responsable et solidaire. Avec Kèpos, je vous propose de mettre mon expérience d'ingénieur, de manager et d'entrepreneuse à votre service pour réfléchir et agir à vos côtés dans votre démarche de transformation"_

## Samuel Colin

Consultant en transition écologique

_"Face à l'urgence climatique, environnementale et sociale, je suis engagé de longue date dans plusieurs mouvements et associations qui œuvrent pour une transition écologique et citoyenne de notre modèle de société. Après une dizaine d'année en tant que chargé de mission en développement durable et en développement économique, j'ai fait le choix en 2016 de quitter la fonction publique territoriale pour me consacrer plus largement à ces engagements. Képos est pour moi l'outil idéal pour faire la passerelle entre les citoyens et les entreprises qui partagent une vision commune d'une société respectueuse des êtres humains et de l'environnement."_

## Ian Mc Laughlin

Chargé du dispositif de la Serre à projets

_"Né le jour de la Saint Patrick, d’un père Britannique et d’une mère Lorraine, je suis ouvert sur le monde. De nature curieux, persévérant et sociable, j’ai baigné tôt dans l’entrepreneuriat social. Je suis également passionné par l’intelligence collective et les projets qui en résultent. Aujourd’hui, au sein de la Serre à projets, je suis heureux d’avoir l’opportunité de me consacrer pleinement aux porteurs de projets en lien avec la transition écologique, de pouvoir leur apporter ma créativité et transmettre mes connaissances du tissu économique local et du monde entrepreneurial."_