---
title-first: Notre
title-last: raison d’être
order: 1
short_description: Notre mission est d’oeuvrer à la transition écologique du territoire
  et de tous ses acteurs.

---
La définition de la raison d'être de Kèpos a été le fruit d'un travail patient et collectif, avec l'ensemble des parties prenantes du projet. En voici quelques jalons.

## Agir à l’heure de l’Anthropocène

Nos organisations humaines font face à des défis auxquels l’humanité n’a jamais été confrontée : réchauffement climatique, effondrement de la biodiversité, épuisement des sols, perte de cohésion de nos sociétés… C’est ce qu’on appelle l’Anthropocène. Tout cela doit redessiner radicalement tous nos modes d’action, de production ou de consommation, si nous voulons collectivement nous assurer un avenir sur cette planète. Nous souhaitons être acteurs de ce changement, nommé transition écologique, par la création et le développement d’activités faisant résolument le choix de la sobriété et du respect des écosystèmes.

## Œuvrer à la transition écologique du territoire

Présents autour de Nancy, en Lorraine, nous sommes un groupe de jeunes entreprises et associations qui nous sommes réunies pour oeuvrer à la transition écologique de notre territoire. Car nous sommes convaincus que le "business as usual" est la stratégie perdante, et que notre responsabilité est d’oeuvrer à développer des activités qui garantissent le respect de la vie humaine et naturelle là où nous sommes installés. Nous contribuons concrètement à cette transition par la transformation complète de nos modèles économiques, en choisissant prioritairement le Bio, les circuits courts, le zéro déchet, le réemploi, le recyclage, les low tech, la sobriété énergétique, la gouvernance partagée...

## Construire un écosystème d’activités de la transition écologique

A travers notre réunion, nous voulons mettre en oeuvre un écosystème d’activités de la transition écologique, qui soit tel un écosystème naturel. Nous animons notre collectif à la manière d’un jardin en permaculture, développant les interactions et les synergies entre des activités qui ne sont ni sur les mêmes marchés, ni sur les mêmes métiers, ni sur les mêmes secteurs. Nous donnons à notre collectif la forme d’une SCIC (Société Coopérative d’Intérêt Collectif), qui est le support de notre ambition. Nous sommes ainsi en mesure d’adresser collectivement un ensemble de besoins qui sont ceux de base à satisfaire dans une logique de transition écologique : se nourrir, se vêtir, se loger ou encore transformer la matière dans le respect des hommes (d’ici et d’ailleurs, d’aujourd’hui et de demain) et des écosystèmes dont nous dépendons pour vivre.