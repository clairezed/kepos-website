---
title-first: Notre
title-last: action
order: 2
short_description: Kèpos est un outil de développement territorial qui intervient
  dans l'émergence, le développement et la transformation d'activités en lien avec
  la transition.

---
Faire émerger de nouvelles activités, contribuer au développement d'entreprises déjà engagées, ou transformer les modèles d'entreprises existantes ou de territoires, voici nos trois modalités d'action principales.

## Faire émerger de nouvelles activités économiques liées à la transition

A travers Kèpos, nous souhaitons contribuer à l’émergence de nouvelles activités de transition sur le territoire. Cela se fait à travers la création et le développement d’une Fabrique à projets d’utilité sociale, opérationnelle depuis septembre 2019, en collaboration avec différents partenaires. Celle-ci apportera les compétences et les ressources nécessaires pour accompagner la création de ces nouvelles activités. Ce dispositif doit permettre de repérer des besoins non satisfaits sur le territoire, d’imaginer des solutions, d’étudier la faisabilité de nouvelles activités en étant issues, et de trouver des porteurs pour les mettre en œuvre. Cet outil de pré-incubation, intitulé [**La serre à projets**](https://www.kepos.fr/serre-a-projets.html "La serre à projets"), est thématisé sur la transition écologique.

## Faire passer des seuils

Notre ambition, en tant que [**jeunes entreprises de la transition**](https://www.kepos.fr/jardin-entreprises/membres.html "Nos membres"), est de nous structurer et de passer des seuils. Nos modèles économiques sont alternatifs, mais nous voulons changer d’échelle pour avoir des impacts sur le territoire, par le jeu de nos relations avec nos clients, nos fournisseurs, nos salariés, nos partenaires…  Pour cela, la SCIC Kèpos, dans le cadre du [**Jardin d'entreprises**](https://www.kepos.fr/jardin-entreprises.html "Le jardin d'entreprises"), utilise trois outils :

* **L’accompagnement** : Alors que nous sommes souvent des entreprises isolées et fragiles, Kèpos nous accompagne pour contribuer à structurer notre développement, fixer une stratégie claire, décrypter notre environnement et orienter notre action. Par l’écoute et le conseil, Kèpos nous aide à affermir notre engagement écologique et à prendre des décisions avisées dans le respect de nos valeurs.
* **La coopération** : nous développons les interactions entre nous pour imbriquer nos activités et former un écosystème le plus productif et résilient possible. Nous échangeons ainsi pratiques, savoir-faire, services ou produits pour enrichir nos compétences et nos métiers. Nous montons des offres communes au service du territoire et de ses acteurs.
* **La mutualisation** : en tant que jeunes entreprises émergentes, nos ressources sont limitées. En nous regroupant, nous nous donnons les moyens d’avoir accès ensemble à des ressources auxquelles seules nous ne pourrions prétendre. Ces ressources à partager peuvent être immobilières, matérielles, humaines, financières, informationnelles et nous permettent de jouer collectif pour aller plus loin.

## Transformer l’existant

En nous réunissant, nous nous donnons les moyens de conjuguer [**nos compétences et nos offres**](https://www.kepos.fr/atelier.html "L'atelier de la transition") afin d’accompagner tout acteur qui le souhaite (entreprise, association, territoire, particulier) dans sa propre transition écologique. Notre ambition est alors de transformer l’existant, en aidant nos clients à changer leurs modèles de développement par l’intégration des impératifs de sobriété, de respect des écosystèmes et de résilience. Pour cela, nous apportons conseils, expertises ou formations, ainsi que tout un panel de produits ou de services (ingénierie, aménagement restauration, développement informatique…). En aidant nos clients à questionner leur mission et leur impact, nous sommes en mesure de transformer des activités dispendieuses et insoutenables en des activités soutenables dans la très longue durée et gagnantes économiquement à court terme. Car nous sommes convaincus qu’intégrer l’écologie dans son activité, c’est impliquer ses collaborateurs, mieux maîtriser son environnement, se différencier, et donc être plus performant économiquement et socialement.