---
layout: page
title-first: Le Jardin
title-last: d'entreprises
permalink: "/jardin-entreprises.html"
category: entreprises
order: 3
short_description: Les entreprises et associations membres de Kèpos forment un jardin
  d’activités qui se développent en synergie.
in_homepage: true

---
Nous, membres de Kèpos, formons un Jardin d’entreprises et d'associations qui se développent en synergie les unes avec les autres. De nos interactions naissent des offres partagées à la disposition des acteurs du territoire.