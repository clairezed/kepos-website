---
layout: page
title-first: Mentions
title-last: légales
permalink: "/mentions-legales.html"
category: mentions
short_description: ''
in_homepage: false
order: 

---
## Editeur

Association Kèpos  
4B Rue des Ecuries  
54500 Vandoeuvre-lès-Nancy

Directeur de la publication : Emmanuel Paul, Président

## Hébergement

Site hébergé chez [SOS Futur](https://www.sos-futur.fr/)

SOS Futur

67, rue Charles Keller

54000 Nancy

France

## Crédits

* Développement du site : [L'assembleuse](https://www.lassembleuse.fr/)
* Icônes : [Fontawesome](https://fontawesome.com/)
* Illustrations : [Caroline Antoine](https://www.caroline-antoine.com/ "Caroline Antoine"), tous droits réservés
* Logo : [lagence235°](http://www.lagence235.com/ "lagence235°")

## Réutilisation des contenus

Sauf mention contraire, les contenus de ce site sont sous licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## Comment créditer les contenus de ce site

L’utilisateur qui souhaite réutiliser les contenus de ce site doit impérativement indiquer :

1. Le titre associé au contenu,
2. La paternité du contenu par la mention suivante : « Source : Kèpos– www.kepos.fr »,
3. Le suivi de la date à laquelle le contenu a été extrait du site : « JJ/MM/AAAA »,
4. L’indication de la licence sous laquelle le contenu a été mis à disposition