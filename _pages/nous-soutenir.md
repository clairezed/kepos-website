---
layout: page
title-first: Nous
title-last: soutenir
permalink: "/nous-soutenir.html"
category: soutenir
statuts: "/scic-kepos-statuts-scic-sas-v220623.pdf"
brochure-investisseurs: "/brochure-kepos.pdf"
souscription_personne_physique: "/scic-kepos-bulletin-souscription-personne-physique-v221026.pdf"
souscription_personne_morale: "/scic-kepos-bulletin-souscription-personne-morale-v221026.pdf"

---
## Pourquoi souscrire ?

Pour mener à bien notre projet, votre  concours est essentiel ! Car Kèpos est avant tout **un projet collectif, ouvert à la participation de chacun** ! Un des meilleurs moyens de nous soutenir est de prendre des parts dans notre Société Coopérative d'Intérêt Collectif (SCIC). **Associés à la gouvernance du projet selon le modèle un homme = une voix**, vous devenez partie prenante de son ambition, en contribuant à son financement et à sa mise en oeuvre concrète par, pourquoi pas, le partage de vos compétences ou de votre réseau. Notre volonté est bien **d'associer le plus largement possible notre projet aux hommes et femmes qui veulent s'engager**.

En souscrivant, vous serez affectés à un collège d'associés soutenant le projet, avec d'un côté les personnes morales, et de l'autre les personnes physiques. Chacun de ces collèges dispose de 10% des droits de vote en assemblée générale.

En outre, si vous souscrivez à titre personnel, vous bénéficierez d'une réduction d'impôt sur le revenu de 25% du montant investi.

## Connaître la SCIC Kèpos

Le montage du projet Kèpos a débuté en janvier 2018. Depuis, de nombreux soutiens ont été recueillis, permettant de confirmer notre ambition et d'affiner notre modèle. Pour vous en rendre compte, nous vous invitons à consulter notre site et les statuts ci-dessous, essentiels pour que vous preniez une décision d'investissement informée.

{% assign doc = page.statuts | remove_first: "/" %}
Télécharger [**les statuts**]({% asset {{doc}} @path %}){:target="_blank"}

{% assign doc = page.brochure-investisseurs | remove_first: "/" %}
Télécharger [**la brochure pour les investisseurs**]({% asset {{doc}} @path %}){:target="_blank"}

## Comment souscrire ?

Vous pouvez maintenant nous renvoyer un bulletin de souscription rempli et signé en deux exemplaires, accompagné de votre règlement par chèque.

**La part sociale est fixé à 100 €. Le minimum de souscription est d'une part pour les personnes physiques, 10 pour les personnes morales**. Il n'y a pas de maximum.

Formulaires de demande de souscription :

{% assign doc = page.souscription_personne_physique | remove_first: "/" %}

* [**pour les personnes physiques**]({% asset {{doc}} @path %}){:target="_blank"}
  {% assign doc = page.souscription_personne_morale | remove_first: "/" %}
* [**pour les personnes morales**]({% asset {{doc}} @path %}){:target="_blank"}

**Adresse d'envoi** :  Kèpos, 4B Rue des Ecuries, 54500 Vandoeuvre-lès-Nancy

<iframe class="mj-w-res-iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://app.mailjet.com/widget/iframe/5qIv/ygq" width="100%"></iframe>