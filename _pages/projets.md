---
layout: page-branch-bg
title-first: La Serre
title-last: à projets
permalink: "/serre-a-projets.html"
category: projets
order: 2
short_description: Kèpos accompagne l’émergence de projets de création d’activités
  en lien avec la transition écologique sur son territoire.
in_homepage: true

---
La transition écologique est un enjeu majeur pour nous tous ! Ensemble, il nous faut œuvrer à un changement très profond de nos modes de vie vers la sobriété et la durabilité. Pour cela, il est essentiel de faire preuve d’imagination pour créer et développer de nouvelles activités qui permettent au territoire d’opérer sa transition. C’est ce que propose [**la Serre à projets**](https://www.laserre.org/) !

Ce dispositif, animé par [**France Active Lorraine**](https://www.franceactive-grandest.org/lorraine/) et la SCIC Kèpos, vise à repérer des besoins non satisfaits sur le territoire pour les transformer en projets de création d'activités économiques. Il s’agit donc bien d’une méthodologie d’entrepreneuriat inversé, où l’on part des besoins du territoire, et non pas des porteurs de projets ! Particularité de cet outil de développement territorial : il est thématisé sur la transition écologique...

# Un processus en 5 étapes

L’action de la Serre à projets se déroule en 5 étapes, renouvelées chaque année :

1. Détecter des besoins sociaux et des opportunités socio-économiques via un réseau de capteurs d’idées.
2. Inventer des réponses collectivement et valider leur pertinence et leur viabilité grâce à une étude d’opportunité.
3. Transmettre le projet à un entrepreneur qualifié ou à une entreprise sociale existante via un appel à candidature.
4. Accompagner les porteurs de projets jusqu’à la création de l’entreprise sociale, après validation des projets grâce à une étude de faisabilité.
5. Lancer la nouvelle activité.

Que vous représentiez une entreprise, une association, un collectif de citoyens ou que vous agissiez à titre individuel, vous pouvez participer au dispositif en faisant remonter vos idées, ou en répondant à l’appel à candidature !

# **Une démarche collective**

Les projets qui sont étudiés dans le cadre de la Serre à projets ont été sélectionné par un comité de pilotage qui associe les partenaires opérationnels et financiers du dispositif. Ceux-ci sont en prise directe avec le territoire et ses acteurs. Son rôle est le suivant :

1. Sélectionner les idées à faire émerger.
2. Suivre les études d’opportunité et lancer l’appel à candidatures.
3. Choisir les porteurs de projets.
4. Valider les études de faisabilité.
5. Soutenir la création de l’activité.

Les projets supportés font donc l’objet d’un accompagnement collectif par l’ensemble des parties prenantes du dispositif.

# **Territoire et cibles**

La Serre à projets est active sur le bassin de vie de Nancy : la Métropole du Grand Nancy au premier chef, et par extension le sud du département de Meurthe-et-Moselle, incluant Toul, Pont-à-Mousson et Lunéville.

La Serre à projets étude l’opportunité et la faisabilité de projets, qu’elle met à la disposition des forces vives du territoire. Entreprises sociales, associations, collectifs de citoyens ou porteurs de projet individuels sont les bienvenus pour se positionner sur les différents projets lors de l’appel à candidature !

# Partenaires

La Serre à projets bénéficie du soutien financier de la Région Grand Est, du Département de Meurthe-et-Moselle, de la Métropole du Grand Nancy, de la Ville de Nancy, du Groupe Ag2R la Mondiale et du Crédit Agricole de Lorraine.

Vous souhaitez avoir plus d'informations sur ce dispositif : découvrez [**le site de la Serre à projets**](https://www.laserre.org/ "La Serre") **!**