---
layout: page
title-first: L'Atelier
title-last: de la transition
permalink: "/atelier.html"
category: cultivons
order: 4
short_description: Kèpos dispose d’une expertise sur les questions de transition qu’elle
  propose aux entreprises et territoires, en Lorraine et ailleurs.
in_homepage: true

---
La transition écologique est une question clé pour la pérennité des entreprises et l'avenir des territoires. Nous sommes **un centre de formation et de ressources,** et nous avons l'ambition de proposer à chacun des éléments de compréhension, des méthodes et des outils pour enclencher son propre processus de transformation.

## Conjuguer nos compétences pour mieux vous accompagner

Avec l'ensemble de ses membres, Kèpos dispose d’une expertise très étendue sur les questions de transition, qu’elle propose aux entreprises et territoires, en Lorraine et ailleurs.

Nous avons construit ensemble une proposition complète d’éveil, de formation et de prestations de services autour de la transition écologique. Pour vous accompagner, selon vos besoins, dans une démarche d'évolution vers des usages plus sobres et soutenables, nous vous proposons une large gamme d'activités et d'actions en lien avec nos compétences. Toutes les interventions sont opérées par des membres de Kèpos professionnels qualifiés. Elles peuvent intéresser :

* les entreprises,
* les associations,
* les collectivités,
* l’éducation nationale,
* l’éducation populaire,
* les organisateurs de festivals et d’événements,
* etc...

Grâce à un financement de la Métropole du Grand Nancy, nous avons construit des catalogues présentant nos offres partagées dans les domaines de la sensibilisation, de la formation, du conseil et des services. Retrouvez également nos réalisations et nos initiatives.