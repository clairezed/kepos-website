document.addEventListener('DOMContentLoaded', function () {
  const wpArticlesContainer = document.getElementById("last-wp-articles")

  if (!!wpArticlesContainer) {
    fetch('https://transition-ecologique.org/wp-json/wp/v2/posts?context=embed&page=1&per_page=3')
      .then(res => {
        // console.log(res);
        return res.json();
      }).then(data => {
        data.forEach((datarecord, idx) => {
          console.log(datarecord);
          // console.log(datarecord.title.rendered);
          let markup = createMarkup(datarecord, idx);
          let container = document.createElement("div");
          container.classList.add("wp-article-wrapper");
          // We make the contents of the container be the result of the function
          container.innerHTML = markup;
          // Append the created markup to the DOM
          wpArticlesContainer.appendChild(container);
          // Show the whole html section 
          const wpArticlesSection = document.getElementById("last-wp-articles-section");
          wpArticlesSection.classList.remove("not-displayed")
        });
      }).catch(err => {
        console.log(err);
      });

  }

});


function createMarkup(datarecord, idx) {
  return `
    <div class="card">
        <div class="card-content">
          <h4 class="title is-5">
            <a href="${datarecord.link}" target="_blank">${datarecord.title.rendered}</a>
          </h4>
          <div class="content">
            ${datarecord.excerpt.rendered}
            <a href="${datarecord.link}" target="_blank">Lire la suite</a>
          </div>
        </div>
      </div>
  `;
}
