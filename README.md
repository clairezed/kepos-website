# Kepos website

- site généré avec Jekyll
- basé sur le [starter jekyll](https://github.com/clairezed/jekyll-starter) de Claire Zuliani
- design fait main en se servant des briques de [Bulma](https://bulma.io/)
- utilisant le CMS [Forestry](https://forestry.io/)


https://www.kepos.fr/