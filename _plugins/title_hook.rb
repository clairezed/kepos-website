module Jekyll
  module KeposTitle

    def self.set_titles(site)
      site.pages.each { |page| join_title(page) }
    end

    def self.join_title(doc)
      if doc['title-first'] && !doc['title']
        doc.data['title'] = [doc['title-first'], doc['title-last']].join(" ")
      end
    end
  end
end

Jekyll::Hooks.register :site, :pre_render, priority: Jekyll::Hooks::PRIORITY_MAP[:high] do |site|
  Jekyll::KeposTitle::set_titles(site)
end